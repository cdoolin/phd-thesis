
Submitting to FGSR

Title:
------
Integrated optical and mechanical resonators for evanescent field sensing

Author:
-------
Doolin, Callum

Keywords:
---------
optical
optics
mechanical
mechanics
resonator
microdisk
whispering-gallery mode
nanostring
silicon nitride
evanescent field
sensing
force sensing
nonlinear
cryogenic
low-temperature
effective mass
tapered fiber
waveguide
quadratic phase
filter
nanofabrication
discrete fourier transform
power spectral density
digital signals
chirp drive

Abstract:
---------
Nanoscale optical and mechanical resonators store energy in a way characterized by a sharp resonance frequency, and through interaction with their surroundings offer a path to the next generation of sensitive measurement tools. In this thesis we investigate a particular geometry of nanofabricated devices-that of monolithically fabricated optical microdisks and nanomechanical resonators, in which the optical microdisk operates as a high-gain amplifier of the mechanical resonator's position.

We began the study with nanoscale silicon microdisks and cantilevers fabricated with a commercial photolithography process for silicon photonics, and used the optomechanical interaction between the cantilevers and optical microdisks to demonstrate readout of the mechanical motion to the fm Hz^-0.5 precision level. This approach has enabled thermally limited readout of forces on the cantilever to 130 +- 40 aN Hz^-0.5 at room temperature, optimized by their nanometer-sized geometry and femtogram-scale masses. We then explored the possibility of using these cantilevers for fundamental quantum measurements of phonon number, and although we concluded the cantilever measurement lacked the necessary characteristics, we developed a framework for characterizing the type of optomechanical coupling exhibited by an optomechanical device.

Continuing on resonator development, we switched to fabricating similar geometry optomechanical devices from silicon nitride, an insulating material used in semiconductor fabrication, known to enable a high quality factor mechanical resonator geometry termed nanostrings. Using a fiber-waveguide coupling technique we were able to optomechanically measure picogram-scale nanostring devices down to temperatures below 1 K, finding mechanical quality factors of 10^6, while exhibiting less optically-induced heating than similar silicon devices. While the optical microdisks enable high-precision readout of mechanical motion, they more generally measure refractive index changes. Using aqueously submerged silicon nitride microdisks, we were able to measure LiCl induced refractive index changes down to the 10^-6 level.

Finally, we carry out a discussion and review on the subject of digital signal processing. Although appearing unrelated, the techniques covered in Chapter 7 underline every single experimental result covered in this thesis. With an understanding of digital signals, flexible and well adapted measurement protocols can be constructed without being stuck relying on the output of fixed-pipeline measurement tools.

Citation for previous publication
---------------------------------

Multidimensional Optomechanical Cantilevers for High Frequency Force Sensing, C. Doolin, P.H. Kim, B.D. Hauer, A.J.R. MacDonald and J.P. Davis, New J. Phys. 16, 035001 (2014). http://dx.doi.org/10.1088/1367-2630/16/3/035001

Nonlinear Optomechanics in the Stationary Regime, C. Doolin, B.D. Hauer, P.H. Kim, A.J.R. MacDonald, H. Ramp and J.P. Davis, Phys. Rev. A 89, 053838 (2014). http://dx.doi.org/10.1103/PhysRevA.89.053838

Refractometric sensing of Li salt with visible-light Si3N4 microdisk resonators, C. Doolin, P. Doolin, B.C. Lewis and J.P. Davis, Appl. Phys. Lett. 106, 081104 (2015). http://dx.doi.org/10.1063/1.4913618

