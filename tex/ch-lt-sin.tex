% !TEX root = ../thesis.tex

% cSpell:ignore acousto, Gooch, Housego


\chapter{
Silicon nitride nanostrings at cryogenic temperatures
\label{ch:lt-sin}
}


\section{Introduction}

Mechanical resonators fabricated from silicon nitride (SiN) have generated significant interest as their intrinsic tensile stress results in abnormally high-quality factors when operated in vacuum \cite{Verbridge2008}. Silicon nitride membranes at cryogenic temperatures have demonstrated quality factors of over $10^8$ \cite{Yuan2015}, while at room temperature SiN trampoline resonators have demonstrated quality factors exceeding $10^7$ \cite{Reinhardt2016, Norte2016}, and more recently phononically engineered SiN nanobeams have demonstrated quality factors of almost $10^9$ at room temperatures \cite{Ghadimi2018}. These devices surpass the room-temperature ``quantum coherent" limit of $Q \times f > 6 \times 10^{12}$, suggesting SiN mechanical resonators may find use in quantum applications without necessitating cryogenic cooling \cite{Norte2016,Ghadimi2018,Aspelmeyer2014}.

At cryogenic temperatures, optomechanical cavities have been used with much success to demonstrate quantum interactions such as entanglement \cite{Palomaki2013, Riedinger2018}, optical and mechanical squeezing \cite{Safavi-Naeini2013, Purdy2013, Wollman2015, Lecocq2015}, and single phonon measurements \cite{Hong2017, Reed2017}. Integrating high-Q SiN mechanical elements in optomechanical cavities \cite{Eichenfield2009, Cohen2013} has obvious upsides for quantum experiments \cite{Purdy2013}, as well for low-noise force \cite{Gavartin2012, Reinhardt2016} and acceleration \cite{Krause2012, St-Gelais2019} sensors.

Recent measurements of silicon \cite{Meenehan2015, Hauer2018} and gallium arsenide \cite{Ramp2019} optomechanical cavities at sub-Kelvin temperatures have demonstrated considerable heating of the mechanics resulting from the optical measurement. Due to the large optical band gap of SiN, we predict SiN optomechanical devices may produce less heating and therefore be well-suited to optical measurements at these low temperatures. We present measurements of optically induced heating at sub-Kelvin temperatures in an integrated optomechanical cavity with the optical and mechanical resonator fabricated from a single layer of high-stress silicon nitride.

\section{Device and methods}

The mechanical device studied here is a silicon nitride nanostring 16 $\upmu$m long and 170 nm in width, fabricated from a 220 nm thick film of high-stress stoichiometric LPCVD silicon nitride---for fabrication details see Appendix \ref{sec:fab:ltsin}. The string exhibits two mechanical modes, an in-plane mode and an out-of-plane mode, both vibrating at $\sim 14$ MHz and having effective masses of $1.3$ pg, as determined through finite-element method (FEM) modeling of the mechanical modes. FEM modeling indicates the in-plane mode exhibits a higher resonance frequency, which we used to provide identification of the modes.

To enable optomechanical readout of the nanostring's motion, the mechanical resonator is fabricated 240 nm away from a 30 $\upmu$m diameter optical microdisk ($Q_\mathrm{opt} \sim 3.8 \times 10^{5}$), such that the mechanical motion is optomechanically transduced into frequency modulations of the microdisk as described theoretically in Chapter \ref{ch:bg}, and similar to the silicon cantilevers described in Chapter \ref{ch:cantilever}. To encourage optomechanical coupling between the nanostring and the microdisk, the mechanical resonator was designed with an arc following $\pi/6$ radians of the microdisk circumference to increase overlap of the optical mode's evanescent field with the nanostring, as can be seen in Figure \ref{fig:lt-sin:diagram}d.


\begin{figure}
	\centerline{\includegraphics[]{fig/ch-lt-sin/fig1_diagram.pdf}}
	\caption{\label{fig:lt-sin:diagram}
	a) Schematic of the experimental setup used to take measurements. Nominally 1550 nm laser light is conditioned with a variable optical attenuator (VOA) and fiber polarization controller (FPC) before being modulated with an acousto-optic modulator (AOM) to control on and off times of the laser with microsecond switching times. A fiber circulator (FC) is used to collect the reflected signal from the device which is measured on a digitizing photodetector (DPD). A digital waveform generator (DIO) is used to send a digital waveform to the AOM and DPD for programming pulse times and synchronization with data collection. b) An optical microscope image of the fiber-coupled optomechanical device. Scalebar 50 $\upmu$m. c) An image of the fiber-coupled device mounted on the cold-head of a 400 mK 3He probe. d) and scanning electron microscope (SEM) image of the mechanical resonator positioned next the the optical microdisk. Scalebar 5 $\upmu$m.
	}
\end{figure}

The nanostring and microdisk are fabricated 200 \um from the cleaved edge of the wafer (see Appendix \ref{sec:fab:ltsin} for details on how this was achieved), with a 1200 nm wide waveguide coupled to the microdisk opposite the nanostring. The waveguide tapers down to a width of 550 nm at the edge of the chip to provide matching of the waveguide mode's effective index of refraction to the that of an optical fiber \cite{Cohen2013, Cohen2015Thesis}, such that a cleaved optical fiber can be positioned in-line with the waveguide to provide coupling with the on-chip optics. Past the optical microdisk, the waveguide is terminated with a photonic crystal mirror such that the waveguide light is reflected and returned back to the same coupling fiber. This reflection from the mirror causes the light to interact with the optical cavity a second time, but in a counter-clockwise direction.  Coupling between the clockwise and counter-clockwise modes, enhanced by the waveguide mirror, results in hybridization and splitting in the frequencies of the optical modes \cite{Kippenberg2002}. This results in a second optical mode being visible in the transmission spectrum---slightly visible in Figure \ref{fig:lt-sin:Gcalc}a.  Throughout this work the optical mode was treated as a single optical mode, however a more advanced treatment considering the coupling between the two optical modes could be considered \cite{Kippenberg2002}. By comparing input optical power to the chip with collected power, a total coupling efficiency of 10\% was measured, or assuming equal loss coupling in-to and out-of of the waveguide, a 32\% coupling efficiency in each direction. An optical microscope image of the fiber-coupled device is shown in Figure \ref{fig:lt-sin:diagram}b. 

As diagramed in Figure \ref{fig:lt-sin:diagram}, measurements were taken by sending light from a 1550 nm tunable diode laser (NewFocus TLB-6330) through an acousto-optic modulator (Gooch \& Housego) to enable microsecond-timescale switching of the optical power to enable the pulsed measurement scheme described later. A wavelength meter picking up 10\% of the laser output was used to provide low-frequency, Hz-rate feedback to the tunable laser to stabilize the tunable laser over the time-scale of days. By using a fiber circulator, reflected light from the device is isolated and detected with a low-noise digitizing photodetector (Resolved Instruments DPD80).

To investigate the low-temperature behavior of the nanostrings, the device was fixed to the cold-head of a $^3$He cryostat capable of cooling to temperatures below 400 mK. By using a fixed fiber-coupling technique, the device is able to attach to low-temperature probes with minimal space, negating the need for any low-temperature positioning stages to perform alignment at cryogenic temperatures, such as done in \cite{MacDonald2015, Kim2016}. However, during cooldown temperature-induced contractions reduced total coupling to about 55\% of initial value, reducing one-way coupling efficiency to $\sim24\%$. A photographic image of the fiber-chip assembly attached to the cold-head is shown in Figure \ref{fig:lt-sin:diagram}c.

For heat-sensitive measurements at fridge temperatures, a pulsing scheme is used that will be further described in Section \ref{sec:lt-sin:ltpulse}. A digital waveform generator (DIO) is used to supply logic signals to the AOM and the digitizing photodetector (DPD) such that data collection of the second pulse is well synchronized. To perform time-resolved measurements of the mechanical resonator's motion, the time-series optical signal collected with the DPD is demodulated at $\Omega$ and filtered to perform a lock-in type measurement to extract amplitude (see Section \ref{sec:dsp:lockin}). A 2 kHz linear-phase filter is used such that no delay is introduced to the processed amplitude signal, allowing precise calibration of pulse turn-on time, however also restricting time-resolution to $\sim500$ \us. To reduce noise, the amplitude signal from 100 pulses are averaged together to perform a measurement of one pulse---such as the curves in Figure \ref{fig:lt-sin:ringup}a. Identical lock-in measurements are performed off resonance to estimate imprecision noise from photon shot-noise and measurement electronics and subtracted.


\begin{figure}
	\centerline{\includegraphics[]{fig/ch-lt-sin/RT_G_calc.pdf}}
	\caption{\label{fig:lt-sin:Gcalc}
	a) DC photodetector signal of the optical transmission as the laser is scanned over an optical resonance at 1572 nm. Due to asymmetries in the optical resonance, only datapoints red-detuned from cavity resonance were used to extract optical parameters (filled purple dots). X-axis is the same across all three subplots. b) The optomechanical gain and AC optical power measured around 14.159 MHz (blue--out-of-plane) and 14.038 MHz (orange--in-plane). Circles represent AC power determined through fitting the mechanical power spectra, while squares indicate AC power determined from direct integration of band-passed power. c) From the optical resonance parameters and the optomechanical gain ($\alpha$), the optomechanical coupling coefficient, $G_1$ can be determined at each optical detuning. The average of the filled in markers was used to extract $G_1/2\pi$ of 73 MHz/nm and 55 MHz/nm for the out-of-plane and in-plane modes respectively.
	}
\end{figure}


We quickly note that given the frequency of the mechanical resonator ($\Omega \sim 14$ MHz) and the temperatures studied here (T $>$ 400 mK), we expect at least 600 phonons to occupy the mechanical mode. In this limit ($n \gg 1$), we can neglect the zero-point motion of the mechanical resonator and treat the number of phonons, $n$, mean squared displacement of the mechanical mode, $\langle x^2 \rangle$, and the effective mode temperature $T$ as linearly proportional measurements of the same quantity---the mode energy: $E = \hbar \Omega n = m_\mathrm{eff} \Omega^2 \langle x^2 \rangle = k_\mathrm{B} T$. Thus even though notation implies phonon number, it may be given in units of temperature and is interchangeable.

\section{Room-temperature characterization}

At room temperature, the optomechanical nanostring-microdisk device was characterized to extract the optomechanical coupling coefficient. Measurements of the thermomechanical motion of the nanostring were taken as the tunable laser was stepped across the optical resonance. In agreement with the tuned-to-the-slope coupling theory presented in Chapter \ref{ch:bg}, the most effective amplification of the mechanical motion was observed slightly off resonance where the slope of the optical resonance is steepest--Figure \ref{fig:lt-sin:Gcalc}. Although the blue-detuned side of the optical resonance disagreed with simple theory due to the presence of the counter-propagating optical mode, the optomechanical gain ($\alpha_\mathrm{max} \sim 30$ W/m) was fit on the red-detuned side of the optical resonance to determine the first-order optomechanical coupling coefficient ($G_1 = \partial \omega / \partial x$) to find $G_1/2\pi = 73 \pm 5$ MHz/nm and $G_1/2\pi = 55 \pm 4$ MHz/nm for the in-plane and out-of-plane modes, respectively. Errors in coupling rates are the standard deviations of the coupling rates due to their slight optical tuning dependance---illustrated by the filled markers in Figure \ref{fig:lt-sin:Gcalc}c.  With zero-point motions of $x_\mathrm{zpf} = 21$ fm, these coefficients result in optomechanical coupling rates ($g_0 = x_\mathrm{zpf} G_1$) of $g_0/2\pi = 1.6 \pm 0.1$ kHz and $g_0/2\pi = 1.2 \pm 0.1$ kHz.

Although this coupling was strong enough to observe thermomechanical motion with a sensitivity of 70 \fmrtHz, we were unable to observe shifts in frequency as a result of the optomechanical spring effect, or changes in mechanical line-width from optomechanical heating or cooling. Compared with the measurements in Chapter \ref{ch:2f}, the optomechanical coupling is approximately 10 times smaller. This smaller coupling may be explained by the relatively large gap between the mechanics and optical disk of 240 nm, and the larger index of refraction of silicon over silicon nitride. Improvements to the device geometry, fine-tuning the fabrication process and implementation of stress-induced gap-narrowing process \cite{Norte2018} should lead to similar devices with improved optomechanical coupling.

As the device is under tension, we do not anticipate any significant changes to the device geometry during cooling to low temperatures and therefore expect the optomechanical coupling to remain constant. Therefore by using the optomechanical coupling coefficient extracted at room temperature and optical resonance parameters extracted at low temperatures, the mean-squared displacement--and thus temperature of the mechanical mode--can be calculated without necessitating calibration with fridge thermometry. In the following sections and figures, whenever number of phonons is reported as a temperature, these are determined through this absolute optomechanical calibration, not through reference with fridge thermometry. 


\section{Low-temperature pulsed measurements}
\label{sec:lt-sin:ltpulse}

As has been previously demonstrated in optomechanical devices fabricated in silicon \cite{Meenehan2015, Hauer2018} and gallium arsenide \cite{Ramp2019}, optical losses at the resonator can contribute significant heating to the mechanical modes. These photons lost from the optical resonator are absorbed, creating a hot phonon bath that couples to and causes heating of the mechanical mode. These previous studies have implemented a pulsed measurement scheme to investigate this heating, whereby the optical drive signal is turned on for a short period and a time-resolved measurement of the mechanical resonator's energy, $n(t)$, is performed.


Following the previous work, this heating process can be modeled as coupling of the nanomechanical mode to two baths: to the hot photon-induced bath $n_\mathrm{p}$ with coupling rate $\Gamma_\mathrm{p}$, and to the cold bath at the base temperature of the fridge $n_\mathrm{th}$ with rate $\Gamma_\mathrm{i}$. This process can be modeled as a first-order differential equation:
\begin{equation}\label{eq:lt-sin:ndot}
	\dot{n}(t) = -\Gamma n(t) + \Gamma_\mathrm{i} n_\mathrm{th} + \Gamma_\mathrm{p} n_\mathrm{p},
\end{equation}
where $\Gamma$ is the total coupling, $\Gamma = \Gamma_\mathrm{i} + \Gamma_\mathrm{p}$. We can solve this equation requiring one known boundary condition, $n(t_0) = n_0$, to find:
\begin{equation}\label{eq:lt-sin:nring}
	n_\mathrm{heat}(t) = n_\mathrm{eq} + \left(n_0 - n_\mathrm{eq}\right)e^{-\Gamma(t - t_0)},
\end{equation}
where we have introduced $n_\mathrm{eq}$ as the steady-state solution to equation \eqref{eq:lt-sin:ndot},
\begin{equation}\label{eq:lt-sin:heat}
	n_\mathrm{eq} = \frac{\Gamma_\mathrm{i} n_\mathrm{th} + \Gamma_\mathrm{p} n_\mathrm{p}}{\Gamma}.
\end{equation}
That is, given the resonator's energy at a point in time, $n(t_0) = n_0$, $n(t - t_0)$ will exponentially decay towards the steady-state energy over a characteristic time $\tau = 1/\Gamma$.


\begin{figure}
	\centerline{\includegraphics[]{fig/ch-lt-sin/fig_ringup.pdf}}
	\caption{\label{fig:lt-sin:ringup}
	a) An example measurement of the probe pulse in the two-pulse measurement scheme described in the text. In-plane mode signal is converted to an effective mode temperature, and number of phonons, using the optomechanical coupling coefficient determined at room temperature. Pulses measured at varying ringdown times are color coded according to the colorbar. Fridge temperature is stabilized to 420 mK. An example fit to equation \eqref{eq:lt-sin:heat} for the pulse with a ringdown time of 0.56 s is overlaid (dashed line). The pulse is fit from 0.5 ms $< t < $ 14.5 ms corresponding to the filter time-constant (vertical dashed line), and $n(t)$ is extrapolated back to $t = 0$ to determine $n_\mathrm{cool}(t_\mathrm{off})$. b) Fitting the exponential decay found from extracting $n_\mathrm{cool}(t_\mathrm{off})$ and $n_\mathrm{eq}$ extracted from the probe measurements, the intrinsic damping $\Gamma_\mathrm{i}$ can be determined. Colored circles correspond to the pulses in a). Orange squares correspond to the out-of-plane mode measured simultaneously with the in-plane mode.
	}
\end{figure}

During the optical measurement, the hot phonon bath described above is present and will act to heat the mechanical mode to $n_\mathrm{eq}$. However, when the optical measurement is turned off, the hot phonon bath and coupling to it, $\Gamma_\mathrm{p}$, will go to zero. In this case, the steady-state occupancy of the mode reduces to the fridge's base temperature $n_\mathrm{th}$, while the total loss rate from the resonator reduces to the intrinsic damping rate $\Gamma_\mathrm{i}$, such that if the resonator starts with $n(t_0) = n_\mathrm{eq}$, it follows
\begin{equation}\label{eq:lt-sin:cool}
	n_\mathrm{cool}(t) = n_\mathrm{th} + (n_\mathrm{eq} - n_\mathrm{th})e^{-\Gamma_\mathrm{i} (t - t_0)},
\end{equation}
such that the mechanical mode cools to the fridge temperature on a time scale of $\tau_\mathrm{i} = 1/\Gamma_\mathrm{i}$.

To measure the intrinsic and hot-bath induced damping rates, a double-pulse measurement is used to implement a pump/probe scheme \cite{Meenehan2015, Hauer2018}. For the pump pulse, the optical measurement is turned on for a period $t_\mathrm{on} = 15$ ms such that the mechanical mode heats up to steady-state conditions. The measurement laser is then switched off for a ring-down time period $t_\mathrm{off}$, after which a second 15 ms measurement--the probe pulse--is performed and recorded. Finally, the optical measurement is then turned off again for a much longer period such that the average laser power into the fridge is $2\%$ of the peak power, and the pump/probe measurement repeated. A set of probe measurements across a range of ringdown times $t_\mathrm{off}$ between pump and probe pulses is shown in Figure \ref{fig:lt-sin:ringup}a.

After the pump pulse is turned off, and before the probe pulse measures the mechanics again, the mechanical mode decays from $n_\mathrm{eq}$ towards $n_\mathrm{th}$ at a rate $\Gamma_\mathrm{i}$ following equation \eqref{eq:lt-sin:cool}. Then during the probe pulse, the mechanical mode heats up from the temperature when the pulse turns on, $n_0 = n_\mathrm{cool}(t_\mathrm{off})$, to the steady state temperature $n_\mathrm{eq}$. By fitting the probe pulse to equation \eqref{eq:lt-sin:heat}, the temperature of the mechanics at the beginning of the probe, $n_0 = n_\mathrm{cool}(t_\mathrm{off})$, and the steady state temperature $n_\mathrm{eq}$, can be found.

By repeating this measurement for a range of $t_\mathrm{off}$, the ringdown behavior of $n_\mathrm{cool}(t_\mathrm{off})$ can be mapped out and fit to equation \eqref{eq:lt-sin:cool} to extract the cold-bath temperature $n_\mathrm{th}$ and the intrinsic dissipation rate of the mechanical resonator, $\Gamma_\mathrm{i}$. Figure \ref{fig:lt-sin:ringup}b plots the ratio of $n_\mathrm{cool}(t_\mathrm{off})$ to $n_\mathrm{eq}$ for the in-plane mode (corresponding to the pulse measurements shown in Figure \ref{fig:lt-sin:ringup}a) measured at a fridge temperature of 420 mK. The intrinsic dissipation for the in-plane mode was found to be $\Gamma_\mathrm{i}/2\pi = 13$ Hz ($Q = 1.1 \times 10^6$) and cold-bath temperature to be $n_\mathrm{th} = 710$ mK.  Note that based on our uncertainty in optomechanical coupling rate due to detuning, we expect an uncertainty level at about $10\%$ in these values.

\section{Discussion}


\begin{figure}
	\centerline{\includegraphics[]{fig/ch-lt-sin/fig_fieldenergy.pdf}}
	\caption{\label{fig:lt-sin:fieldenergy}
	a) The cold-bath temperatures, $n_\mathrm{th}$--orange, and hot photon-induced bath temperature, $n_\mathrm{p}$--blue, plotted against intracavity photon number. Mode temperature is determined using room-temperature optomechanical coupling with low-temperature optical resonance fits to determine optomechanical gain to provide absolute calibration of the mechanical resonators displacement, and via the equipartition theorem, the mode's energy. Fridge temperature was at 420 mK for all measurements. Filled circles represent the out-of-plane mode, while open circles represent the in-plane mode. Dashed lines are linear fits to both in-plane and out-of-plane points for guides to the eye. b) The dissipation from the mechanical mode to hot photon-induced bath (blue) and to the cold fridge bath (orange). Again dashed lines are linear fits to both in-plane and out-of-plane datapoints.
	}
\end{figure}


Although the cold-bath temperature of 710 mK measured in Figure \ref{fig:lt-sin:ringup} does not agree precisely with fridge thermometry (420 mK),
the measured temperature is determined completely through calibration of the optomechanical transduction and not based off fridge thermometry. The only external temperature reference used is the room temperature (295 K) used during the thermomechanical calibration illustrated in Figure \ref{fig:lt-sin:Gcalc}.
Despite temperature discrepancies, the device is able to maintain reasonable temperature accuracy over two orders of magnitude of temperature change. We attribute this to the monolithic fabrication of waveguide, optical resonator, and mechanical resonator providing relatively-fixed optomechanical parameters.

The mismatch of the mechanical mode temperature with the fridge thermometry may be an indication the mechanical device is not well thermalized to the cold plate of the fridge, inaccuracies in the optomechanical calibration, or a combination of the two. To further investigate this, measurements were taken while the laser was set to a variety of detunings on the red-side of the optical resonance to monitor how intra-cavity photon number in the microdisk affected heating of the nanostring. Measurements of the cold and hot phonon bath temperatures, $n_\mathrm{th}$ and $n_\mathrm{p}$, and coupling to those baths, $\Gamma_\mathrm{i}$ and $\Gamma_\mathrm{p}$, as a function of intracavity phonon number are shown in Figure \ref{fig:lt-sin:fieldenergy}.

We found both the hot-bath temperature $n_\mathrm{p}$ and cold bath temperature $n_\mathrm{th}$ showed positive correlations to intracavity photon number, indicating the cold phonon bath was not fully thermalized to the fridge temperatures. Interestingly, the in-plane and out-of-plane modes exhibited consistently different hot-bath temperatures (cold-bath temperatures remained in agreement), with the out-of-plane mode exhibiting an $n_\mathrm{p}$ 1.5 times that of the in-plane mode despite exhibiting an optomechanical coupling 1.3 times smaller than the in-plane mode.  This implies that material properties and geometry play a dominant role in hot-bath temperatures compared with optomechanical coupling. Cold-bath temperatures remained consistent between in-plane and out-of-plane modes, and as shown in Figure \ref{fig:lt-sin:fieldenergy}b, there did not seem to be a significant correlation between intracavity phonon number and bath coupling rates. 

Next, we explored the fridge temperature dependence by stabilizing the fridge to several temperature set-points up to 700 mK, maintaining $\sim11000$ intracavity photons---Figure \ref{fig:lt-sin:fridgetemp}. The intrinsic mechanical dissipation rate $\Gamma_i$ (Figure \ref{fig:lt-sin:fridgetemp}b), of which measurement does not depend on absolute optomechanical calibration, does show a trend of decreasing with temperature, however as measurement is restricted to a temperature range of only a few hundred mK, measurements over a larger temperature range will be required to make definitive claims of the physical mechanism. 

Contrary to what is expected, the measured cold-bath phonon mode temperatures displayed an inverse correlation to measured fridge temperatures (Figure \ref{fig:lt-sin:fridgetemp}a). While this may indicate improper thermalization to the fridge, because the 500 mK, 600 mK, and 700 mK cold-phonon bath temperatures are all measured to be less than the fridge temperatures, this indicates that the optomechanical gain calibration is inaccurate.

\begin{figure}
	\centerline{\includegraphics[]{fig/ch-lt-sin/fig_fridgetemp.pdf}}
	\caption{\label{fig:lt-sin:fridgetemp}
	a) The cold-bath temperatures, $n_\mathrm{th}$--orange, and hot photon-induced bath temperature, $n_\mathrm{p}$--blue, plotted against fridge temperature. Mode temperature is determined using room-temperature optomechanical coupling with low-temperature optical resonance fits to determine optomechanical gain to provide absolute calibration of the mechanical resonators displacement, and via the equipartition theorem, the mode's energy. Filled circles represent the out-of-plane mode, while open circles represent the in-plane mode. Dashed lines are linear fits to both in-plane and out-of-plane points for guides to the eye. b) The dissipation from the mechanical mode to hot photon-induced bath (blue) and to the cold fridge bath (orange). Dashed lines are linear fits to both in-plane and out-of-plane datapoints.
	}
\end{figure}



The optomechanical gain $\alpha$, given by equation \eqref{eq:bg:omgain} and typically on the order of $\alpha \approx 30$ W/m for these measurements, depends on a number of device parameters:
\begin{equation}
	\alpha = \frac{4 \kappa_e \kappa_0 G_1 \bar{s}^2}{\kappa^3} \frac{2 \Delta/\kappa} {(1 + (\Delta/\kappa)^2)^2}.
\end{equation}
Here $\bar{s}^2 = 67 \times 10^{12}$ photons/s ($8.5\;\upmu$W) is the off-resonance optical power travelling through the waveguide, $\kappa_e$ and $\kappa_0$ are the waveguide coupling and intrinsic loss rates of the microdisk ($\kappa_e/2\pi = 0.6$ GHz), $\kappa = \kappa_e + \kappa_0$ is the total microdisk loss rate ($\kappa /2\pi = 2.3$ GHz), and $\Delta$ is the laser detuning from the optical resonance frequency. Because each set of ringdown measurements were taken over a couple day time period, that provides ample time for drift in these parameters, skewing calculation of $\alpha$. By better characterizing the optical resonance, carefully keeping track of parameter drifts by interweaving optical disk characterization with the pulse measurements, we expect to be able to better accurately determine $\alpha$ and therefore $n_\mathrm{th}$ to improve the optomechanical thermometry. Further, using a homodyne detection scheme where the laser is locked to optical resonance may also help eliminate drift in optomechanical transduction. 



\section{Conclusion}

The picogram-scale silicon nitride nanostrings described in this chapter display a number of desirable characteristics that warrant further interest in high-stress silicon nitride optomechanical devices at sub Kelvin temperatures. 
With an optical power of a 8.5 $\upmu$W coupling to the microdisk, heating of the mechanical resonator only reaches a couple Kelvin (a couple thousand phonons) in mode temperature. Compared with the similar frequency and geometry silicon devices characterized by Hauer \textit{et al.}~which heat up to $\sim 80$ K ($8 \times 10^4$ phonons) \cite{Hauer2018}, these devices present an noticeable improvement in optically-induced heating. We suggest this is due to the larger optical bandgap of silicon nitride \cite{Philipp1973}, although the much lower optomechanical coupling of our devices may also play a large role--although in the modes studied here, optomechanical coupling did not correspond to the degree of optically-induced heating.

Quality factor wise, the best $Q = 10^6$ observed for the strings measured here match or exceed those of the silicon devices measured by Hauer \textit{et al.}~at similar fridge temperatures \cite{Hauer2018}. For the silicon nitride membranes measured at sub Kelvin temperatures by Yuan \textit{et al}, they report a sharp increase in quality factor below 200 mK \cite{Yuan2015}, suggesting lower temperatures may improve quality factors further--although device dimensions and mass differ by a factor of about 1000. By expanding the temperature range of measurements over from mK to 10s of K, the quality factor dependence of these pg-scale nanostrings could better be characterized.

