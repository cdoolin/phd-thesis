% !TEX root = ../thesis.tex

% cSpell:ignore labstuff, AFMs, bilayers, Gavartin, IMEC, Srinivasan, piezoresistively, MRFM

%
% adapted from labstuff/2013_cantilever_njp_john
%


% \usepackage{graphicx} %eps figures can be used instead
% \usepackage{bm}
% \usepackage{xspace} 

% \newcommand{\um}{$\mathrm{\mu m}$\xspace}
% \newcommand{\rtHz}{${\rm Hz}^{-1/2}$\xspace}
% \newcommand{\aNrtHz}{${\rm aN} / \sqrt{\rm Hz}$\xspace}
% \newcommand{\fmrtHz}{${\rm fm} / \sqrt{\rm Hz}$\xspace}
% \newcommand{\Sx}{$S_{zz}$\xspace}
% \newcommand{\Sf}{$S_{FF}$\xspace}
% \newcommand{\Sfth}{$S_{FF}^{\rm th}$\xspace}
% \newcommand{\Sv}{$S_{VV}$\xspace}
% \usepackage{epstopdf}




\chapter[Multidimensional optomechanical cantilevers for high frequency force sensing]{Multidimensional optomechanical cantilevers for high frequency force sensing
\footnote{This chapter has appeared in ``Multidimensional optomechanical cantilevers for high frequency force sensing" \cite{Doolin2014cantilever}}
}
\label{ch:cantilever}


\section{Introduction}

The atomic force microscope (AFM) \cite{Binnig1986}, has become an indispensable tool for probing the physical characteristics of microscopic systems.
Working by Hooke's Law, $F = - k x$, the tip of the AFM (measured at position $x$) is displaced proportional to an applied force, $F$, transducing forces into a detectable signal. This effect has been used to great effect for surface imaging, where interatomic forces between an AFM tip and substrate are measured as raster images of the surface structures down to the atomic scale \cite{Giessibl1995} and beyond \cite{Gross2009}.
The ability to use AFMs in liquid environments \cite{Fukuma2005} has led to their widespread use in biological applications \cite{Sharma13}, such as live imaging of biological specimens \cite{Muller2008}, and non-scanning applications like studying receptor-ligand binding of surface proteins \cite{Viani2000} and deciphering the mechanics of proteins through unfolding experiments \cite{Rief1997, Brockwell2003}. For applications such as these where it is desirable to monitor the dynamics of the system with great time resolution, the bandwidth of the measurement process becomes critical. 

High-speed AFM, the use of MHz frequency resonators \cite{Ando2001}, has enabled the dynamics of molecular systems to be visualized at speeds of up to 80 ms for a $50 \times 100$ pixel image \cite{Uchihashi2011}. This technology has permitted the real-time imaging of individual motor proteins \cite{Uchihashi2011}, proteins diffusing and interacting in lipid bilayers \cite{Casuso2010}, and the folding of synthetic DNA origami structures \cite{Endo2009}. When operated dynamically \cite{Albrecht1991}, the maximum time resolution of the measurement is limited by the frequencies of the structural modes of the cantilever. In the simple harmonic approximation, these frequencies are $\Omega_0 = \sqrt{ {k} / {m_{\rm eff}} } $, where $k$ and $m_{\rm eff}$ are the spring constant and effective mass of a particular mode, as described in Section \ref{sec:bg:meff} \cite{Hauer2013}. Therefore AFMs with small masses, or large spring constants, grant access to the regime of large bandwidth and exceptional time resolution through increased mechanical frequencies.

The force sensitivity of a mechanical resonator is limited by the thermal forces acting on the resonator. From the fluctuation-dissipation theorem these forces have spectral densities
\begin{align} 
{S}_{F}^{\rm th} &= 4 k_B T m_\mathrm{eff} \Gamma \\
			&= 4 k_B T \frac{m_{\rm eff} \Omega}{Q} \\
			&= 4 k_B T \frac{k}{\Omega Q},
\end{align}
where $k_B$ is the Boltzmann constant, $T$ is the bath temperature, and $Q = \Omega_0 / \Gamma$ is the mechanical quality factor \cite{Gavartin2012}.
With this equation in mind, the thermal noise on a force sensor can be minimized in two general ways: by reducing the spring constants of the devices, or by reducing the effective masses. Single-crystal silicon cantilevers with low spring constants ($10^{-6}$ N/m) have long since demonstrated aN\rtHz force sensitivities at cryogenic temperatures \cite{Mamin2001}. However, the small $k$ results in lowered mechanical frequencies, limiting the time resolution of the measurements. On the other hand, reducing the effective masses of resonators typically increases their mechanical frequencies. 
Further, small dimensions lessen the effect viscous damping has on the reduction of the mechanical $Q$ \cite{Verbridge2008}, and thus reduce thermal forces.
Therefore minimizing the dimensions, and $m_{\rm eff}$, grants access to the regime of both delicate force sensing and exceptional time resolution through increased mechanical frequencies. 

Today's nanofabrication tools, in particular electron beam lithography (EBL), allow for the design of mechanical resonators with nanometer dimensions and effective masses of picograms or less. Nanomechanical resonators described by Li \textit{et al.} have demonstrated room temperature force sensitivities of 510 aN\rtHz in vacuum and 1300 aN\rtHz in air \cite{Li2007}. Using a stressed silicon nitride resonator to provide large mechanical quality factors, Gavartin \textit{et al.} have demonstrated a vacuum room temperature force sensitivity of $74$ aN\rtHz \cite{Gavartin2012}. Deserving special mention are bottom-up fabricated force sensors using carbon nanotube resonators \cite{Jensen2008, Moser2013}, and silicon nanowires \cite{Nichol2008}, which owing to their tiny effective masses ($\sim10^{-20}$~kg) have demonstrated unprecedented force sensitivity approaching the zN\rtHz level at cryogenic temperatures \cite{Moser2013}.  Since the original preparation of this chapter, SiN trampoline resonators have demonstrated force noise to below 20 aN\rtHz \cite{Reinhardt2016}, while top-down fabricated graphene resonators have shown force noise to 16 aN\rtHz \cite{Kumar2015}.

The force sensing ability of an AFM is dependent on the properties---$m_{\rm eff}$, $\Omega_0$, $Q_\mathrm{mech}$ (geometry, material)---of its mechanical resonator. However, to perform measurements with the AFM, a detection method is required to observe the motion of the resonator. While AFMs generally gain better force sensitivity as dimensions are decreased, the task of detecting the displacement of the resonator becomes more challenging. Two common methods to detect the displacement of a cantilever are reflecting a laser beam off the cantilever onto a position sensitive photodetector, termed optical beam deflection (OBD), or recombining the reflected beam interferometrically. However, as mentioned in Chapter \ref{ch:intro}, these detection methods scale poorly as the dimensions of the nanomechanical devices fall below the spot size of the laser beam ($\ge$ 1 \um) \cite{Eki05}, creating an effective limit on detectable cantilever sizes (and frequencies) that has already been reached by modern nanomechanical resonators.

Cavity optomechanics \cite{Kippenberg2007, Eichenfield2009, Anetsberger2009} offers excellent displacement sensitivity while being well suited for nanoscale devices. By spatially localizing optical cavity modes with a mechanical resonator, motional degrees of freedom are coupled to frequency (or phase) shifts of the optical modes. These frequency shifts can be carefully monitored, demonstrated by experiments measuring the imprecision in the motion of nanomechanical resonators to the standard quantum limit (SQL)---the theoretical noise floor of a continuous measurement determined from dynamical back-action and photodetector shot noise \cite{Anetsberger2010}.

%An important benchmark of a displacement detection system is the displacement noise floor: the noise corresponding to the minimum displacement resolvable by the detection system. OBD has obtained displacement noise floors of 5 fm\rtHz \cite{Fukuma2009}, while an all-fiber interferometer has achieved noise floors of 2 fm\rtHz \cite{Rasool2010}, both with standard low-frequency cantilevers ($\sim$ 300 kHz). By placing a nanomechanical resonator in the evanescent field of an optical microdisk resonator, and detuning the probe laser to the slope of the optical resonance, displacement noise floors of $\approx$ 0.2 fm\rtHz have been observed \cite{Anetsberger2009, Liu2012}.

\section{Results and discussion}

Here, three sizes of low mass, MHz frequency, optomechanical devices suited to AFM applications are presented. They consist of cantilever-style nanomechanical resonators coupled to the whispering gallery modes of optical microdisks and are commercially fabricated from a 215 nm thick silicon layer of a silicon-on-insulator (SOI) wafer, ensuring simple fabrication with automatic and reproducible optomechanical cavity formation. 
For fabrication details see Appendix \ref{sec:fab:cantilever}.
The cantilevers have lengths of 8, 4, and 2 \um, and are on average 400 nm wide, broadening towards the end to allow functionalization without compromising the optical cavity quality ($Q_{\rm opt}$\,$\sim$\,3\,$\times$\,$10^4$ for 20 \um diameter disk). They couple to disks of 20, 10 and 5 \um diameter, respectively. Scanning electron microscopy (SEM) images and finite element method (FEM) simulations of the first three structural modes of the 8 \um long cantilever are shown in Figure \ref{fig.devs}.
Devices are measured both in vacuum ($10^{-5}$ torr) and air to investigate force sensitivities in different environments.
We envision single-molecule force (folding/unfolding) experiments as the ideal AFM application for these devices, as this would not degrade the optical $Q$ of micro disk due to a sample, nor would a separate tip need to be attached.

\begin{figure}
	%%%%%%%%%%%%%%% FIGURE 1 %%%%%%%%%%%%%%%%%
	\centerline{\includegraphics[width=5 in]{fig/ch-cantilever/fig1.eps}}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\caption{{\label{fig.devs}} (a) SEM image of the optomechanical device with a 20 \um diameter optical microdisk evanescently coupled to an 8 \um long cantilever. Coordinates are aligned such that $\hat{x}$ is parallel to the axis of the cantilever, $\hat{y}$ points along in-plane motion of the cantilever, and $\hat{z}$ points out-of-plane. (b) 10 \um disk, 4 \um cantilever and (c) 5 \um disk, 2 \um cantilever; scale bars 5 \um on all panels. (d)-(f) FEM simulations reveal the first three modes of the 8 \um long cantilever as an example: an out-of-plane mode, an in-plane mode and a second out-of-plane mode. Mechanical modes of the shorter cantilevers are similar. Color scale indicates relative displacement.
	}
\end{figure}

To measure the motion of our device's cantilever, single-mode light from a tunable diode laser (New Focus TLB-6330, 1550-1630 nm) is passed through a dimpled, tapered optical fiber \cite{Mic07} placed on the top edge of the optical microdisk opposite to the mechanical device using three-axis nanopositioning stages (Figure \ref{fig.how}b). By slightly detuning the laser from an optical resonance of the disk, modulations in the frequency of the optical modes induced by the movement of the mechanical resonator are transduced to a voltage signal from a photodetector (PD) measuring the transmission through the tapered fiber. A lock-in amplifier (Zurich H2FLI) is used to measure the high-frequency spectral density of the PD voltage (\Sv), which is then thermomechanically calibrated to displacements of the cantilever's tip using $m_{\rm eff}$ determined from FEM simulations \cite{Hauer2013}.  
%Based on uncertainties exact fabricated device dimensions, we estimate a $10\%$ uncertainty in the effective mass.


\begin{figure*}
	%%%%%%%%%%%%%%% FIGURE 2 %%%%%%%%%%%%%%%%%
	\centerline{\includegraphics[width=1.0\textwidth]{fig/ch-cantilever/fig_how.eps}}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\caption[(a) Schematic of experimental setup (VOA - variable optical attenuator, FPC - fiber polarization controller, HPF - high pass filter, LPF - low pass filter, PD - photodiode). (b) Optical image of a dimpled, tapered fiber placed on a 10 \um diameter disk opposite an 8 \um long cantilever; scale bar 20 \um. (c) Transmission (normalized to transmission in the absence of coupling) through the tapered fiber (black), while simultaneously locked-on to the out-of-plane mode (light blue) and the in-plane mode (yellow), reveals the maximum peak signal occurs slightly detuned from the optical resonance, approximately corresponding to the maximum slope of the transmission. (d) Scanning the entire frequency range of the tunable laser reveals optical resonances that provide maximum signal.
	]
	{{\label{fig.how}} (a) Schematic of experimental setup (VOA - variable optical attenuator, FPC - fiber polarization controller, HPF - high pass filter, LPF - low pass filter, PD - photodiode). (b) Optical image of a dimpled, tapered fiber placed on a 10 \um diameter disk opposite an 8 \um long cantilever; scale bar 20 \um. (c) Transmission (normalized to transmission in the absence of coupling) through the tapered fiber (black), while simultaneously locked-on to the out-of-plane mode (light blue) and the in-plane mode (yellow), reveals the maximum peak signal occurs slightly detuned from the optical resonance, approximately corresponding to the maximum slope of the transmission \cite{Kim2013}. (d) Scanning the entire frequency range of the tunable laser reveals optical resonances that provide maximum signal.
	}
\end{figure*}
	

In all cases, peaks in the voltage spectral density corresponding to thermodynamic actuation of the fundamental out-of-plane mode were visible, but when measured in vacuum the in-plane modes, and the second out-of-plane mode of the 8 \um cantilever (Figure \ref{fig.devs}f), were additionally visible. Actuation using a broadband longitudinal piezo buzzer revealed that the lowest-frequency mode was more efficiently actuated, resulting in its identification as the out-of-plane mode.  However, as piezo buzzers will shake the entire mechanical system, potentially activating orthogonally moving modes, we cannot be completely sure of mode identification. 

When calculating the effective mass of the cantilevers through FEM simulation, dimensions where extracted from SEM images of the cantilevers.  For the most critical dimensions---the thinnest widths of the cantilevers---we estimate maximum errors in dimension of $30\%$.  When comparing frequencies estimated from FEM simulations to frequencies of the measured modes, we find less an average error of $28\%$.  From this we can limit the error in effective mass, and therefore the displacement noise floor and force noise sensitivity, to $30\%$.

With this estimation of error in mind, displacement noise floors of $2.0\,\pm\,0.6$ fm\rtHz were observed for the out-of-plane motion of the 4 \um cantilever, equivalent to the best noise floors observed using traditional AFM detection methods \cite{Fukuma2009, Rasool2010}, yet for these radically smaller, lighter, and higher-frequency cantilevers.

The small displacement noise floors achieved with these devices are a result of the efficiency with which displacements of the cantilever are transduced into frequency changes in the optical disk, coupled with the narrow line-widths of the optical resonances. This efficiency can be described to first order by the optomechanical coupling coefficient, $G_1 = d\omega_0 / dz$, where $\omega_0$ is the optical cavity frequency. The small gap between the cantilever and the optical microdisk ($\approx 130$ nm) enables good optomechanical coupling. In addition, the cantilevers curve with the microdisk to optimize $G_1$ by increasing overlap between the optical whispering gallery modes and the cantilever's motion (Table \ref{thetable}). In all devices, the out-of-plane motion of the cantilever had considerably better optomechanical coupling than the in-plane motion, resulting in the difference in displacement noise floors between Figures \ref{fig.peaks}a and \ref{fig.peaks}c, the spectral densities of the 8 \um cantilever's two first modes. The apparent symmetry of the out-of-plane motion might suggest a small linear optomechanical coupling for the out-of-plane mode, however slanted sidewalls of the devices due to fabrication (Figures \ref{fig.couple}a, \ref{fig.couple}b), the placement of the dimpled fiber touching the top of the optical disk introduce sufficient asymmetries, or even incorrect identification of mechanical modes explain the large linear optomechanical coupling observed \cite{Kim2013}.

\begin{figure*}
	%%%%%%%%%%%%%%% FIGURE 3 %%%%%%%%%%%%%%%%
	\centerline{\includegraphics[width=1.1\textwidth]{fig/ch-cantilever/fig_peaks.eps}}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\caption{{\label{fig.peaks}} (a) A peak in the displacement noise density, $\sqrt{S_{z}}$, corresponding to out-of-plane motion of the 8 \um cantilever. The peak at higher frequency is the in-plane mode. $\sqrt{S_{z}}$ is fit to a superposition (blue) of the thermal noise of the cantilever ($\sqrt{S_{z}^{\rm th}}$, red dashed) and a constant measurement noise, the displacement noise floor ($\sqrt{S_{z}^{\rm nf}}$, green). (b) By dividing $\sqrt{S_{z}}$ by the force susceptibility, $| \chi(\omega)|$, the measured force noise density, $\sqrt{S_{F}}$, can be obtained. (c) $\sqrt{S_{y}}$ and (d) $\sqrt{S_{F}}$, corresponding to the 8 \um cantilever's in-plane plane mode, and (e), (f) second out-of-plane mode. In all cases $\sqrt{S_{F}}$ is limited by thermal forces when at the cantilever resonance frequency and limited by detector noise off-resonance.
	(g) $\sqrt{S_{z}}$, and (h) $\sqrt{S_{F}}$ of the 4 \um device's out-of-plane mode are dominated by thermal noise across a wide frequency range due to the low optomechanical detection noise floor. Shown in light brown are $\sqrt{S_{z}}$ and $\sqrt{S_{F}}$ in air, with corresponding fits in dark brown. $\sqrt{S_{z}^{\rm nf}}$ in air agrees with that in vacuum, but $\sqrt{S_{F}^{\rm th}}$ is limited to 2 fN\rtHz due to the viscous damping, compared with 180 aN\rtHz. 
	% Further data is shown in the online supporting data.}
	}
\end{figure*}



\begin{figure}
	%%%%%%%%%%%%%%% FIGURE 4 %%%%%%%%%%%%%%%%
	\centerline{\includegraphics[width=1.1\textwidth]{fig/ch-cantilever/fig_om.eps}}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\caption{{\label{fig.couple}} (a) Tilted SEM image of a device with a 4 \um cantilever; scale bar 500 nm. Side walls have a slope of approximately 10$^{\rm o}$ from vertical, creating asymmetries in the optomechanical coupling. (b) FEM simulation of an optical mode in cylindrically symmetric coordinates. Color bar indicates the relative log magnitude of the electric field. (c) The blue-detuned laser power going into the tapered fiber to detect the 4 \um device causes stiffening of the cantilever. The frequency of the out-of-plane motion increased by $\sim\,0.1$\%, while the in-plane motion showed negligible effect due to its $\sim 100 \times$ smaller $G_1$, Table \ref{thetable}. Errors in Power and Frequency shift are similar to the marker size. The optomechanically induced frequency shift of the cantilever is dependent on the wavelength of light used to detect the device's motion and the maximum positive frequency shift is plotted. (d) An example measurement of the voltage spectral density measured as the laser is scanned towards larger wavelengths. Darker colors indicate larger spectral density (log scaled). The darkened data point in (c) corresponds to the data shown in (d).
	}
\end{figure}

The linear susceptibility, $\chi(\omega)=z(\omega) / F(\omega)$, relates displacements of the cantilever's tip, $z(\omega)$, to applied forces, $F(\omega)$. By dividing the measured displacement spectral density by $|\chi(\omega)|^2$, the observed force spectral density can be found (Figure \ref{fig.peaks}b, d, f, and h).
The thermal forces on the cantilever impose a minimum force sensitivity, and in all cases in which the thermomechanical motion of the cantilever was detected, the total force noise reached a minimum at the cantilever resonant frequency equal to the thermal noise, ${S}_{F}^{\rm th}$. In vacuum, both the in-plane and out-of-plane modes of the 8 \um cantilever, exhibited the best observed force sensitivity of $130\,\pm\,40$ aN\rtHz, Figures \ref{fig.peaks}b, \ref{fig.peaks}d. However in air, the situation was reversed and the 2 \um cantilever presented best force sensitivity of 1500 aN\rtHz.

While the devices presented here don't set a record for any individual metric, we believe they provide an excellent candidate for carrying out force measurements in certain regimes, namely the sensitive measurement of forces at room temperature and atmospheric pressures. The smallest (m$_{\rm eff}$ = 50 ag) piezoresistively detected nanomechanical resonator of Li \textit{et al.}~\cite{Li2007} achieves a force sensitivity of 1300 aN\rtHz, not much smaller then the 1500 aN\rtHz achieved with our smallest device, but with slightly less displacement resolution (39 fm\rtHz) than with our optomechanical detection mechanism (18 fm\rtHz). Alternatively, the in-air, optomechanically detected, doubly clamped beam geometry of Srinivasan \textit{et al.} \cite{Srinivasan2011} and Liu \textit{et al.}~\cite{Liu2012} achieve 10x better displacement noise floors, but fall short on force sensitivity (4400 aN\rtHz vs. 1500 aN\rtHz).

Optomechanically detected silicon nitride resonators represent the best micromachined (\textit{i.e.}~not grown like carbon nanotubes \cite{Moser2013} or silicon nanowires \cite{Nichol2008}) room-temperature force-sensors reported in the literature \cite{Gavartin2012, Reinhardt2016, Norte2016, Ghadimi2018}. However, these devices gain their force sensing ability from the high intrinsic (\textit{i.e.}~in vacuum) quality factors of high-stress silicon nitride strings \cite{Verbridge2008}. Since viscous dissipation mechanisms present in air would likely dominate over the intrinsic vacuum dissipation mechanisms, we imagine that at atmospheric pressure nanostrings would lose their quality factor advantage, and because of the much larger $m_{\rm eff}$ of the strings (9 pg) versus the devices presented here (0.14 pg), our devices may well achieve better force sensitivity. Further, all three of the above compared force sensors were fabricated using electron beam lithography. Devices presented in this chapter were fabricated at a commercial foundry (IMEC) using deep UV lithography, a process much better suited to the commercial fabrication of many such devices. We imagine EBL could be used to produce optomechanically detected cantilevers similar to those presented here, but with $m_{\rm eff}$ similar to those presented by Li \textit{et al.}, providing equivalent force sensitivities, but with possibly better displacement noise floors.


\begin{table*}[t]
	\begin{adjustbox}{width=1.2\textwidth,center=\textwidth}
	\begin{tabular}{ l @{\hskip 3pt} l *{7}{c} }
	% L M K f0 Q Sx Sf G
	\multicolumn{2}{l}{Cantilever Length} & $m_{\rm eff}$ & $k$ & $\Omega_0 / 2\pi$ & $Q$ (air) & $\sqrt{S_{z,y}^{\rm nf}}$ (air) & $\sqrt{S_{F}^{\rm th}}$  (air) & $G_1$ \\
	\multicolumn{2}{c}{$[\upmu {\rm m}]$} & $[{\rm fg}]$ & $[$N/m$]$ & $[$MHz$]$ &  & $[$fm\rtHz$]$ & $[$aN\rtHz$]$ & $[$MHz / nm$]$ \\
	\hline
	\hline
	% G01_0
	2 & out-of-plane  & 140 & 2.2     & 20.1 & 3,600 (120) &    20 (18)  &       290 (1,500)      & 2,000         \\
	% G01_1
	2  &  in-plane & 180 & 3.3     & 21.4 & 5,000           &   120  &           280          & 340            \\
	%G04_0
	4  & out-of-plane & 240 & 0.30   & 5.43 & 4,300 (35)   &      2 (3)    &        180 (2,000)     & 720   \\
	%G04_1
	4  &  in-plane & 260 & 0.48   & 7.04 & 4,400          &    300    &           200          & 6              \\
	%G08_0
	8  &  out-of-plane & 610 & 0.087 & 1.90 & 6,500 (22)  &      18 (17)   &       135  (2,300)     & 150            \\
	%G08_1
	8  &  in-plane & 610 & 0.11   & 2.18 & 7,800          &    390    &            132         & 7                 \\
	%G08_2
	8  &  2$^{\rm nd}$ out-of-plane & 610 & 13      & 23.2 &      5,600     &    55     &              510        & 57                \\
	
	\end{tabular}
	\end{adjustbox}
	\caption
	[Measured parameters of investigated devices. Data is presented for three optomechanical devices of varying size, but similar geometry (Figure \ref{fig.devs}), with cantilevers approximately 2, 4, and 8 \um long. For each device at least two different mechanical modes were detected. Effective masses ($m_{\rm eff}$) for each mode were computed from dimensions measured with SEM, using FEM to determine the mode shape.  Effective masses were calculated based on displacements of the cantilever measured at the tip of the cantilever. Peaks were then thermomechanically calibrated to extract $\Omega_0$, the cantilever's resonance frequency, $Q$, the mechanical quality factor in vacuum, and $S_{z}^{\rm nf}$, the displacement noise floor. From these parameters we compute $k$, the mode's spring constant, and \Sfth, the spectral density of thermal forces on the cantilever imposing a force sensing limit. When measured in air, the quality factors of the cantilevers were reduced by viscous damping and only the out-of plane motion could be detected thermomechanically. Smaller cantilevers exhibited the larger quality factors in air, and smaller thermal forces, resulting in better force sensing ability - opposite to the case in vacuum. 
	] % Remove citation from List of Tables
	{{\label{thetable}} Measured parameters of investigated devices. Data is presented for three optomechanical devices of varying size, but similar geometry (Figure \ref{fig.devs}), with cantilevers approximately 2, 4, and 8 \um long. For each device at least two different mechanical modes were detected. Effective masses ($m_{\rm eff}$) for each mode were computed from dimensions measured with SEM, using FEM to determine the mode shape \cite{Hauer2013}. Peaks were thermomechanically calibrated to extract $\Omega_0$, the cantilever's resonance frequency, $Q$, the mechanical quality factor in vacuum, and $S_{z}^{\rm nf}$, the displacement noise floor. From these parameters we compute $k$, the mode's spring constant, and \Sfth, the spectral density of thermal forces on the cantilever imposing a force sensing limit. When measured in air, the quality factors of the cantilevers were reduced by viscous damping and only the out-of plane motion could be detected thermomechanically. Smaller cantilevers exhibited the larger quality factors in air, and smaller thermal forces, resulting in better force sensing ability---opposite to the case in vacuum. } 
\end{table*}
	
While \Sfth was reached regardless of detector noise, low displacement noise floors broadened the frequency range over which thermally limited force noise was observed (\textit{e.g.}~Figure \ref{fig.peaks}b vs.~\ref{fig.peaks}h). Therefore small displacement noise floors, while not reducing the minimum force sensitivity, allow for larger bandwidth (faster) force measurements.
%Larger bandwidth could also be achieved with feedback \cite{Gavartin2012}, or additional signal processing \cite{Harris2013}, broadening the width of the peaks without affecting \Sfth, and allowing wide bandwidth measurements at the thermal noise level for fast scanning \cite{Mertz1993}.

Operating an AFM at low bath temperatures would reduce thermal noise on the cantilevers, as described by the fluctuation-dissipation theorem. Accordingly, the best force sensitivities have been reached on devices at cryogenic temperatures. Assuming device parameters ($m_{\rm eff}$, $\Omega$, $Q_{\rm vac}$) remain constant across temperatures, a thermal force noise of 3 aN\rtHz at 100 mK is expected to be detectable above the room temperature displacement noise floors of the 8 \um cantilever's out-of-plane motion. This is comparable with the $0.5$ aN\rtHz force sensitivity detectable by a conceptually similar superconducting microwave resonator \cite{Teufel2009}, or the $0.8$ aN\rtHz sensitivity of kHz frequency cantilevers used for magnetic resonance force microscopy (MRFM) \cite{Mamin2001}. Thus we propose optomechanically detected nanomechanical resonators are also good candidates for low-temperature, high-frequency, precision force measurements.

While the optomechanical coupling allows readout of the cantilever's position by monitoring the optical resonator, the optical resonator provides radiation pressure back-action on the mechanical device affecting its dynamics. Because the time scale of optical cavity relaxation is much quicker than the mechanical response time $\tau \sim 2 \pi / \Omega$ (\textit{i.e.} quite unresolved sideband regime), the radiation pressure forces provide only an optical spring effect, allowing the tuning of $k$ to within $\sim 0.1 \%$ (Figure \ref{fig.couple}c), as opposed to any optomechanical heating or cooling \cite{Arcizet2006}. 

\section{Conclusion}

Optomechanical AFMs provide the path to ultra-sensitive molecular force probe spectroscopy, HS-AFM, and other AFM applications. By comparing three different sized force sensing devices, we have demonstrated a trade off in force sensing ability between low spring constant and low effective mass devices depending on the application of interest: the larger, low spring constant device provided best force sensing in vacuum, but the smaller devices excelled in a viscous environment. We have demonstrated optomechanical detection of sub-picogram effective mass multidimensional AFM cantilevers that are commercially fabricated, with displacement noise floors down to $2.0\,\pm\,0.6$ fm\rtHz, and $130\,\pm\,40$ aN\rtHz force sensitivity in vacuum at room temperature. Challenges remain, including selective attachment of relevant molecules, yet we envision that extension of the devices presented here to aqueous environments will open new doors in high-speed, high-resolution molecular force measurements.

% TODO: put acknowledgments somewhere?
%\ack

%The authors wish to thank University of Alberta, Faculty of Science; the Canada Foundation for Innovation; the Natural Sciences and Engineering Research Council, Canada; and Alberta Innovates Technology Futures for their generous support of this research. We also thank Greg Popowich for technical support, along with Mark Freeman, Wayne Hiebert and Paul Barclay for helpful discussions. % cSpell:disable-line




