
LATEXFILE=thesis-pu.tex
LATEXMK_OPTS=-pdf   -pv-

# -auxdir=build

thesis: 
	latexmk $(LATEXMK_OPTS) $(LATEXFILE)

tidy:
	latexmk $(LATEXMK_OPTS) -c

clean:
	latexmk $(LATEXMK_OPTS) -C
	rm -rf build

.PHONY=thesis clean tidy